def issue_to_html(data: dict, when: str) -> str:
    username = data["username"]
    company = data["company"]
    email = data["email"]
    title = data["title"]
    issue = "<br>".join(data["body"].split("\n"))
    return (
        f"<p><b>User</b>: {username}<br>"
        f"<b>Company</b>: {company}<br>"
        f"<b>Email</b>: {email}<br>"
        f"<b>When</b>: {when}<br></p>"
        f"<p><b>Title</b>: {title}<br>"
        "<b>Description</b>:<br>"
        f"{issue}</p>"
    )


def issue_to_str(data: dict, when: str) -> str:
    username = data["username"]
    company = data["company"]
    email = data["email"]
    return (
        f"FROM: {username} ({email}) from {company}\n\n"
        "Description:\n\n"
        f"{data['body']}"
    )
