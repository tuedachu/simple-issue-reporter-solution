# Simple Issue Reporter Solution

[![language](https://img.shields.io/badge/python-3.7-green.svg)](https://www.python.org/downloads/release/python-370/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![version](https://img.shields.io/badge/version-1.0.0-green.svg)]()


This issue reporter uses a simple html form to send data (username,
email, issue, comment and eventually file) from a web client to a
server.

The server creates a new issue in bitbucket, eventually attaches the
file, and send an email (eventually attaching the file) to the support
team.

## Technologies

- The solution uses html/css/javascript.

- The sever is built using `flask`.

- The requests to `bitbucket` api are done using the `requests`
  package. Sending emails is done using the `email` and `smtplib`
  python packages.


## Installation

1. Clone the repository
2. Configure the following environment variables:
```
# Bitbucket
export BITBUCKET_USER=<your_bitbucket_username>
export BITBUCKET_PWD=<your_bitbucket_password>
# For a repo https://bitbucket.org/username/project_name
# Just use BITBUCKET_PATH = username/project_name
export BITBUCKET_PATH=<path_to_your_bitbucket_repository>

# Email
export EMAIL_SERVER=<your_email_server>
export EMAIL_SERVER_PORT=<your_email_server_port>
export EMAIL_USERNAME=<your_email_username>
export EMAIL_PWD=<your_email_password>
export SUPPORT_EMAIL=<email_of_the_support_team>
export SOFTWARE_NAME=<software_name> # This is used in the subject of the email

# Flask
export FLASK_APP=server.py
export FLASK_ENV=development
```
3. Run the application:
```
flask run
```
4. Open a web browser at `localhost:5000`


