import smtplib
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
import re
import os
from os.path import basename
import logging
from formatting import issue_to_html

logger = logging.getLogger(__name__)


EMAIL_SERVER = os.getenv("EMAIL_SERVER")
EMAIL_SERVER_PORT = os.getenv("EMAIL_SERVER_PORT")
EMAIL_USERNAME = os.getenv("EMAIL_USERNAME")
EMAIL_PWD = os.getenv("EMAIL_PWD")
SUPPORT_EMAIL = os.getenv("SUPPORT_EMAIL")
SOFTWARE = os.getenv("SOFTWARE_NAME")


def create_email(
    address: str, subject: str = None, cc: str = None, body: str = None
) -> str:
    url = f"mailto:{address}"
    sep = "?"
    if subject:
        url += sep
        sep = "&"
        url += "subject="
        url += re.sub(" ", "%20", subject)
    if cc:
        url += sep + "cc="
        url += re.sub(" ", "%20", cc)
    if body:
        url += sep
        url += "body="
        url += re.sub("\n", "%0A", re.sub(" ", "%20", body))
    return url


def send_email_to_support_team(
    data: dict, issue_url: str, issue_id: int, when: str, attachment: str = None
):
    logger.info("Sending email to support team")
    sendto = SUPPORT_EMAIL
    smtpserver = smtplib.SMTP(EMAIL_SERVER, EMAIL_SERVER_PORT)
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.ehlo
    smtpserver.login(EMAIL_USERNAME, EMAIL_PWD)

    username = data["username"]
    company = data["company"]
    email = data["email"]
    title = data["title"]
    email_url = create_email(
        address=email,
        subject=f"[{SOFTWARE}] Regarding your ticket: {title}",
        cc=SUPPORT_EMAIL,
        body=(
            f"Dear {username},\n\n"
            f"Please refer to issue #{issue_id} in further communication.\n\n"
            "Best regards,\n\n"
        ),
    )

    msg = MIMEMultipart()
    if attachment:
        logger.info(f"Adding attachment: {attachment}")
        with open(attachment, "rb") as fil:
            part = MIMEApplication(fil.read(), Name=basename(attachment))
        # After the file is closed
        part["Content-Disposition"] = 'attachment; filename="%s"' % basename(attachment)
        msg.attach(part)

    msg.attach(
        MIMEText(
            (
                "Hi support team,<br><br>"
                f"A new issue was created by {username} at {company} {when}<br><br>"
                f"Bitbucket issue can be accessed <a href='{issue_url}'> here</a>. "
                "Do not forget to assign it to somebody.<br><br>"
                "<blockquote><br>"
                f"{issue_to_html(data, when)}"
                "<br></blockquote>"
                "<br><br>"
                """
    <table width="100%" cellspacing="0" cellpadding="0">
      <tr>
          <td>
              <table cellspacing="0" cellpadding="0">
                  <tr>
                      <td style="border-radius: 2px;" bgcolor="#ED2939">
    """
                f'<a href="{email_url}" target="_blank" style="padding: 8px 12px; border: 1px solid #ED2939;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #ffffff;text-decoration: none;font-weight:bold;display: inline-block;">'
                """
                              Reply
                          </a>
                      </td>
                  </tr>
              </table>
          </td>
      </tr>
    </table>
    """
            ),
            "html",
        )
    )

    msg["Subject"] = f"[{SOFTWARE}] New Issue: #{issue_id} -- {title}"
    msg["From"] = EMAIL_USERNAME
    msg["To"] = sendto

    smtpserver.send_message(msg)
    smtpserver.close()
