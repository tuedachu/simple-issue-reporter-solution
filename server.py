import os
import logging
from flask import Flask, request, render_template
from werkzeug.utils import secure_filename
from datetime import datetime
import json

import bitbucket
from email_sender import send_email_to_support_team

logger = logging.getLogger(__name__)

logging.basicConfig(level=logging.INFO)

UPLOAD_FOLDER = "files"
ALLOWED_EXTENSIONS = {"pdf", "png", "jpg", "jpeg", "xls", "xlsx", "mkv"}

app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
app.config["MAX_CONTENT_LENGTH"] = 5 * 1024 * 1024  # in MB


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


def handle_request(data: dict, attachment: bool = False, filename: str = None):
    when = datetime.now().strftime("on %d-%m-%Y at %H:%M:%S")
    issue_id = bitbucket.add_issue(data, when)
    if attachment:
        bitbucket.add_issue_attachment(issue_id, filename)
    issue_url = bitbucket.get_issue_link(issue_id)
    send_email_to_support_team(data, issue_url, issue_id, when, filename)
    logger.info(f"Deleting {filename}...")
    os.delete(filename)


@app.route("/", methods=["GET"])
def root_method():
    return render_template("index.html")


@app.route("/", methods=["POST"])
def upload_file():
    logger.info("Receiving client side request")
    save_file = False
    filename = None
    if "file" in request.files:
        logger.debug("An attachment was sent")
        file = request.files["file"]
        save_file = True
        if file.filename == "":
            logger.warning("No selected file")
            save_file = False
        if not allowed_file(file.filename):
            logger.warning("File extension not permitted")
            save_file = False
        if save_file:
            logger.info("Saving file...")
            filename = secure_filename(file.filename)
            filename = os.path.join(app.config["UPLOAD_FOLDER"], filename)
            file.save(filename)

    data = json.loads(request.form["data"])
    logger.info(data)
    response = app.response_class(status=200, mimetype="application/json")
    handle_request(data, save_file, filename)
    return response


if __name__ == "__main__":
    app.run(debug=True)
