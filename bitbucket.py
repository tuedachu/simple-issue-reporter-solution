import requests
import json
import os
import logging
from formatting import issue_to_html, issue_to_str

logger = logging.getLogger(__name__)


BITBUCKET_USER = os.getenv("BITBUCKET_USER")
BITBUCKET_PWD = os.getenv("BITBUCKET_PWD")
BITBUCKET_PATH = os.getenv("BITBUCKET_PATH")
BITBUCKET_API_URL = "https://api.bitbucket.org/2.0/repositories/" + BITBUCKET_PATH


def add_issue_attachment(issue_id: int, filepath: str):
    logger.info(f"Uploading {filepath} to bitbucket issue #{issue_id}")
    requests.post(
        f"{BITBUCKET_API_URL}/issues/{issue_id}/attachments",
        auth=(BITBUCKET_USER, BITBUCKET_PWD),
        files={"attachment": open(filepath, "rb"),},
    )


def get_issue_link(id: int) -> str:
    return requests.get(
        f"{BITBUCKET_API_URL}/issues/{id}", auth=(BITBUCKET_USER, BITBUCKET_PWD),
    ).json()["links"]["html"]["href"]


def add_issue(data: dict, when) -> int:
    logger.info(f"Adding issue to {BITBUCKET_PATH}")
    headers = {
        "Content-Type": "application/json",
    }
    r = requests.post(
        f"{BITBUCKET_API_URL}/issues",
        auth=(BITBUCKET_USER, BITBUCKET_PWD),
        data=json.dumps(
            {
                "title": data["title"],
                "kind": data["issuetype"],
                "content": {
                    "html": issue_to_html(data, when),
                    "markup": "markdown",
                    "raw": issue_to_str(data, when),
                    "type": "rendered",
                },
            }
        ),
        headers=headers,
    ).json()
    return r["id"]
